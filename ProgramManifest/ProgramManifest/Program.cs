﻿using CommandLine;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;

namespace ProgramManifest
{

    class Program
    {
        public class Options
        {
            [Option('v', "verbose", Required = false, HelpText = "Set output to verbose messages.")]
            public bool Verbose { get; set; }

            [Option("filename", Required = false, Default = @"./program_manifest.json", HelpText = "output filename.")]
            public string filename { get; set; }
        }

        public class Prog
        {
            public string name;
            public short state;
            public string vendor;
            public string version;
        }

        public class Manifest
        {
            public List<Prog> progs;
        }

        public static void Write_Program_Manifest(string manifest_output_path)
        {
            // https://docs.microsoft.com/en-us/previous-versions/windows/desktop/legacy/aa394378(v%3Dvs.85)
            ManagementObjectSearcher mos = new ManagementObjectSearcher("SELECT * FROM Win32_Product");
            //Console.WriteLine("[");

            // serialize JSON to a string and then write string to a file
            //File.WriteAllText(@"c:\movie.json", JsonConvert.SerializeObject(Prog));

            // serialize JSON directly to a file
            using (StreamWriter file = File.CreateText(manifest_output_path))
            {
                // https://www.newtonsoft.com/json/help/html/T_Newtonsoft_Json_JsonConvert.htm
                JsonSerializer serializer = new JsonSerializer();
                serializer.Formatting = Formatting.Indented;
                // https://docs.microsoft.com/en-us/previous-versions/windows/desktop/legacy/aa394378(v%3Dvs.85)

                var manifest = new Manifest();
                manifest.progs = new List<Prog>();
                foreach (ManagementObject mo in mos.Get())
                {
                    var p = new Prog
                    {
                        name = (string)mo["Name"],
                        state = (short)mo["InstallState"],
                        vendor = (string)mo["Vendor"],
                        version = (string)mo["Version"]
                    };
                    manifest.progs.Add(p);
                    //serializer.Serialize(file, p);
                    //Console.WriteLine("{ name: \"{0}\", state: \"{1}\", version: \"{2}\" }", mo["Name"], mo["InstallState"], mo["Version"]);
                }
                serializer.Serialize(file, manifest);
            }
            //Console.WriteLine("]");
            //int x = 0;
        }

        public static void Compare_Program_Manifests(string manifest1, string manifest2)
        {
            //TODO Add a compare function to output differences to a log file.
        }

        static void Main(string[] args)
        {
            Parser.Default.ParseArguments<Options>(args)
                .WithParsed<Options>(o =>
                {
                    if (o.Verbose)
                    {
                        Console.WriteLine($"Verbose output enabled. Current Arguments: -v {o.Verbose}, Filename = '{o.filename}'");
                        Console.WriteLine("Quick Start Example! App is in Verbose mode!");
                    }
                    else
                    {
                        Console.WriteLine($"Current Arguments: -v {o.Verbose}, Filename = '{o.filename}'");
                        Console.WriteLine("Quick Start Example!");
                    }
                    Write_Program_Manifest(o.filename);
                });

        }
    }
}
